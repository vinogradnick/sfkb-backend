import mongoose from 'mongoose';
import dotenv from 'dotenv';
import { v4 as uuidGen } from 'uuid';
import moment from 'moment';
import faker from 'faker/locale/ru';
import ScheduleModel from '../src/schedule/schema/schedule';
import DoctorsModel from '../src/doctors/schema/doctors';
import UsersModel from '../src/users/schema/users';

dotenv.config();

mongoose.connect(
  `mongodb+srv://sfkb:${process.env.MONGO_PASSWORD}@cluster0.dhgtj.mongodb.net/${process.env.MONGO_DATABASE}?retryWrites=true&w=majority`,
  { useCreateIndex: true },
);

/**
 *
 */
async function migrate() {
  const db = mongoose.connection;
  // eslint-disable-next-line camelcase
  // const slot_ids = new Array(100).fill(undefined).map(() => uuidGen());
  // const doctors = new Array(5).fill(undefined).map(
  //   (_, idx) =>
  //     new models.DoctorsModel({
  //       id: uuidGen(),
  //       name: faker.name.firstName(),
  //       spec: 'Терапевт',
  //       slots: slot_ids.slice(idx * 5, (idx + 1) * 5),
  //     }),
  // );
  // await db.collection('doctors').insertMany(doctors);

  // const slots = new Array(100).fill(undefined).map(
  //   (_, idx) =>
  //     new models.SchedulesModel({
  //       uuid: slot_ids[idx],
  //       slot: moment()
  //         .add('hours', idx + 1)
  //         .unix(),
  //       doctor_id: doctors[idx],
  //     }),
  // );
  const slots = [];
  const doctors = [];
  for (let i = 0; i < 100; i++) {
    const doctorId = uuidGen();
    let counter = 0;
    const slotIds = new Array(10).fill(undefined).map(() => uuidGen());
    for (const slotId of slotIds) {
      const date = moment()
        .add('h', counter + 1)
        .unix();
      console.log({
        date,
        df: moment.unix(date).format('YYYY-MM-DD HH:mm:ss'),
      });
      const slot = new ScheduleModel({
        uuid: slotId,
        slot: date,
        doctor_id: doctorId,
      });
      counter += 1;
      slots.push(slot);
    }

    const doctor = new DoctorsModel({
      id: uuidGen(),
      name: faker.name.firstName(),
      spec: 'Терапевт',
      slots: slotIds,
    });
    doctors.push(doctor);
  }

  await db.collection('schedules').insertMany(slots);
  await db.collection('doctors').insertMany(doctors);
  const users = new Array(10).fill(undefined).map(
    () =>
      new UsersModel({
        id: uuidGen(),
        name: faker.name.firstName(),
        phone: faker.phone.phoneNumber(),
      }),
  );
  await db.collection('users').insertMany(users);
}

migrate().then((status) => {
  console.log('MIGRATION SUCCESS');
  process.exit();
});
