
Для запуска создать .env файл


```
MONGO_USER=sfkb
MONGO_PASSWORD=HtRS3HJa2UhVY7fy
MONGO_DATABASE=sfkb

```

## Запуск проекта

```bash
yarn start
```

## Запуск миграций

```
yarn migrate
```


### MongoShell Connect


```
mongo "mongodb+srv://cluster0.dhgtj.mongodb.net/sfkb" --username sfkb
```


## Запросы


## Добавление записи

POST: http://localhost:4000/schedule

### Пример данных
```
{
    "slot": 1625729406,
    "user_id": "f8743f99-855a-474f-9f9d-81e56bd11e34",
    "doctor_id": "d95b1c92-838e-437b-9e20-41068fba74e3"
}
```

## Просмотр забронированных записей

GET:
http://localhost:4000/schedule/scheduled