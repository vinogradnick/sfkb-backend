/**
 * @typedef ScheduleDto
 * @property {string} user_id
 * @property {string} doctor_id
 * @property {Date|string|number} slot
 */

import moment from 'moment';
import { UsersService } from '../users/users.service';

/**
 * @private {mongoose} db
 */
export class ScheduleService {
  /**
   * @param {{
   * SchedulesModel:Model
   * }} models
   * @param {{
   * userService:UsersService}} services
   */
  constructor(models, services) {
    this.dbModel = models;
    this.services = services;
    this.findByRange = this.findByRange.bind(this);
  }

  /**
   *
   * @param {ScheduleDto} scheduleDto
   */
  async postSchedule(scheduleDto) {
    const slot = await this.hasSlot(scheduleDto.doctor_id, scheduleDto.slot);

    if (slot) {
      const user = await this.services.userService.getOne(scheduleDto.user_id);
      if (user) {
        await this.dbModel.SchedulesModel.update(
          { uuid: slot.uuid },
          { user_id: scheduleDto.user_id, status: 'scheduled' },
        );
        return await this.getOne(slot.uuid);
      } else {
        return { type: 'notfound.error', error: 'NotFoundUser' };
      }
    }
    return {
      type: 'schedule.error',
      error: 'Schedule slot is invalid or scheduled',
    };
  }
  /**
   *
   * @param {string} uuidSlot

   */
  async getOne(uuidSlot) {
    return await this.dbModel.SchedulesModel.findOne({ uuid: uuidSlot });
  }
  /**
   * find all  schedule modelsl
   * @return {any[]}
   */
  getAll() {
    return this.dbModel.SchedulesModel.find({});
  }
  /**
   * find all  schedule modelsl by status
   * @param {'open'|'scheduled'} status
   * @return {any[]}
   */
  async getAllByStatus(status) {
    return this.dbModel.SchedulesModel.find({ status });
  }
  /**
   *
   * @param {string} doctorId
   * @param {number} slot
   * @return {DoctorsModel}
   */
  async hasSlot(doctorId, slot) {
    return this.dbModel.SchedulesModel.findOne({
      doctor_id: doctorId,
      slot,
      status: 'open',
    });
  }
  /**
   * find slots by range and status 'scheduled'
   * @param {number} lteDate
   * @param {number} gteDate
   * @return {any[]}
   */
  async findByRange(lteDate, gteDate) {
    console.log({
      lte: lteDate,
      gte: gteDate,
      lteDate: moment.unix(lteDate).format('YYYY-MM-DD HH:mm:ss'),
      gteDate: moment.unix(gteDate).format('YYYY-MM-DD HH:mm:ss'),
    });
    return this.dbModel.SchedulesModel.find({
      slot: {
        $lte: lteDate,
        $gte: gteDate,
      },
      status: 'scheduled',
    });
  }
}
