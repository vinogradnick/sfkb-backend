import { v4 as uuidGen } from 'uuid';
import mongoose from 'mongoose';

const ScheduleSlotSchema = new mongoose.Schema({
  uuid: { type: String, default: () => uuidGen() },
  slot: { type: Number, required: true },
  user_id: { type: String, required: false },
  doctor_id: { type: String, required: true },
  status: {
    type: String,
    enum: ['open', 'scheduled'],
    default: 'open',
  },
});
export default mongoose.model('schedules', ScheduleSlotSchema);
