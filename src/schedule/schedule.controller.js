import express from 'express';
import validator from 'isvalid';
import ScheduleError from './errors/schedule.errors';

/**
 * Data transfer object to schedule
 * @typedef ScheduleDto
 * @property {string} user_id
 * @property {string} doctor_id
 * @property {Date|string|number} slot
 */
const ScheduleDto = {
  user_id: { type: String, required: true },
  doctor_id: { type: String, required: true },
  slot: { type: Number, required: true },
};

/**
 * Api router service
 * @param {any} service
 * @return {express.Router} router of api
 */
function ScheduleController({ ScheduleService }) {
  this.name = 'Schedule';
  const router = new express.Router();
  router.use((req, res, next) => {
    req.service = ScheduleService;
    next();
  });

  router.get('/', async (req, res) => {
    const resp = await req.service.getAll();
    return res.status(200).json(resp);
  });

  router.get('/:status', async (req, res) => {
    const status = req.params.status;
    if (status === 'open' || status === 'scheduled') {
      const resp = await req.service.getAllByStatus(status);
      return res.status(200).json(resp);
    }

    return res.status(400);
  });

  router.post('/', async (req, res) => {
    const reqData = await validator(req.body, ScheduleDto);
    const resData = await req.service.postSchedule(reqData);
    if (resData.error) {
      return res.status(422).json(resData);
    }
    return res.status(200).json(resData);
  });

  return router;
}
export default ScheduleController;
