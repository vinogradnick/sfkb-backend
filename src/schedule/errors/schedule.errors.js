/**
 *
 * @param {string} message
 * @return {{code:number,message:string}}
 */
export default function ScheduleError(message) {
  return { message: message };
}
