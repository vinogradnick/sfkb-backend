import { usersService } from '../users/users.module';
import ScheduleController from './schedule.controller';
import { ScheduleService } from './schedule.service';
import ScheduleModel from './schema/schedule';

export const scheduleService = new ScheduleService({
  SchedulesModel: ScheduleModel,
  usersService: usersService,
});
export const scheduleModule = new ScheduleController({
  ScheduleService: scheduleService,
});
