import express from 'express';
import morgan from 'morgan';
import dotenv from 'dotenv';
import { scheduleModule } from './schedule/schedule.module';
import mongoProvider from './providers/mongo.provider';
import { notificationModule } from './notification/notification.module';
import { usersModule } from './users/users.module';
dotenv.config();

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(
  morgan(':method :url :status :res[content-length] - :response-time ms'),
);
mongoProvider();
app.use('/schedule', scheduleModule);

app.use('/users', usersModule);
notificationModule.init();

app.listen(4000, () => {
  console.log('server started at port 4000');
});
export default app;
