/**
 *
 */
export class DoctorsService {
  /**
   * @param {{
   * DoctorsModel:Model,
   * }} models
   */
  constructor(models) {
    this.dbModel = models;
    this.getOne = this.getOne.bind(this);
    this.getMany = this.getMany.bind(this);
  }
  /**
   *l
   * @param {any[]} uuidList
   * @return {any[]}
   */
  async getMany(uuidList) {
    return this.dbModel.DoctorsModel.find({ id: { $in: uuidList } });
  }
  /**
   *
   * @param {string} id
   * @return {any}
   */
  async getOne(id) {
    return this.dbModel.DoctorsModel.find({ id: id });
  }
  /**
   *
   * @param {string} id
   * @param {any} data
   * @return {any}
   */
  async update(id, data) {
    await this.dbModel.DoctorsModel.updateOne({ id }, data);
    return await this.getOne(id);
  }
}
