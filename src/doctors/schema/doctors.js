import { v4 as uuidGen } from 'uuid';
import mongoose from 'mongoose';
const DoctorsSchema = new mongoose.Schema({
  id: {
    type: String,
    default: () => uuidGen(),
    required: true,
    uniqure: true,
  },
  spec: { type: String, required: false },
  name: { type: String, required: false },
  slots: [String],
});

export default mongoose.model('doctors', DoctorsSchema);
