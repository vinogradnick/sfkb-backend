import { DoctorsService } from './doctors.service';
import DoctorsModel from './schema/doctors';

export const doctorsService = new DoctorsService({
  DoctorsModel: DoctorsModel,
});
