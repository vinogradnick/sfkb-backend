import express from 'express';

/**
 * Api router service
 * @param {any} service
 * @return {express.Router} router of api
 */
function UsersController({ UsersService }) {
  const router = new express.Router();
  router.use((req, res, next) => {
    req.service = UsersService;
    next();
  });

  router.get('/', async (req, res) => {
    const resp = await req.service.getAll();
    return res.status(200).json(resp);
  });
  router.get('/:uuid', async (req, res) => {
    const resp = await req.service.getOne(req.params.uuid);
    return res.status(200).json(resp);
  });

  return router;
}
export default UsersController;
