import UsersModel from './schema/users';
import UsersController from './users.controller';
import { UsersService } from './users.service';

export const usersService = new UsersService({ UsersModel: UsersModel });
export const usersModule = new UsersController({ UsersService: usersService });
