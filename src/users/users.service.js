/**
 *
 */
export class UsersService {
  /**
   * @param {{
   * UsersModel:Model,
   * }} models
   */
  constructor(models) {
    this.dbModel = models;
    this.getOne = this.getOne.bind(this);
    this.getMany = this.getMany.bind(this);
  }

  /**
   *
   * @return {any[]}
   */
  async getAll() {
    return this.dbModel.UsersModel.find({});
  }
  /**
   *
   * @param {string} uuid
   * @return {any}
   */
  async getOne(uuid) {
    return this.dbModel.UsersModel.findOne({ id: uuid });
  }
  /**
   *l
   * @param {any[]} uuidList
   * @return {any[]}
   */
  async getMany(uuidList) {
    return this.dbModel.UsersModel.find({ id: { $in: uuidList } });
  }
  /**
   *
   * @param {string} id
   * @param {any} data
   * @return {any}
   */
  async update(id, data) {
    await this.dbModel.UsersModel.updateOne({ id: id }, data);
    return await this.getOne(id);
  }
}
