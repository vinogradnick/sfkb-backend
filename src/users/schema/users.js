import { v4 as uuidGen } from 'uuid';
import mongoose from 'mongoose';
const UsersSchema = new mongoose.Schema({
  id: {
    type: String,
    default: () => uuidGen(),
    required: true,
    uniqure: true,
  },
  phone: { type: String, required: false },
  name: { type: String, required: false },
});

export default mongoose.model('users', UsersSchema);
