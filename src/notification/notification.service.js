// eslint-disable-next-line no-unused-vars
import moment from 'moment';
import writeNotification from '../providers/notification.provider';

/**
 * @typedef ScheduleDto
 * @property {string} user_id
 * @property {string} doctor_id
 * @property {Date|string|number} slot
 */

/**
 * @private {mongoose} db
 */
export class NotificationService {
  /**
   *

   * @param {any} services
   * @param {any} modules
   */
  constructor(services) {
    this.services = services;
    this.notify = this.notify.bind(this);
    this.notificationHour = this.notificationHour.bind(this);
    this.notificationDay = this.notificationDay.bind(this);
  }
  /**
   *
   * @param {string} currentDate
   * @param {any[]} collection
   */
  async notify(currentDate, collection, users, doctors, isNext) {
    console.log('NOTIFY', { currentDate });
    for (const obj of collection) {
      const user = users.find((user) => user.id === obj.user_id);
      const doctor = doctors.find((doctor) => doctor.id === obj.doctor_id);
      console.log({ user, doctor });
      if (user && doctor) {
        const messageNext = `${currentDate} | Привет ${
          user.name
        }! Напоминаем что вы записаны к ${doctor.spec} ${
          doctor.name
        } завтра в ${moment.unix(obj.slot).format('HH:mm')}!
`;
        const messageNow = `${currentDate} | Привет ${
          user.name
        }! Вам через 2 часа к ${doctor.spec} ${doctor.name}  в ${moment
          .unix(obj.slot)
          .format('HH:mm')}!
`;
        await writeNotification(isNext ? messageNext : messageNow);
      }
    }
  }
  /**
   *
   */
  async notificationHour() {
    const currentDate = moment().format('YYYY-MM-DD HH:mm:ss');
    const lteDate = moment().add('h', 2).add('minute', 2).unix();
    const gteDate = moment().add('h', 2).unix();
    const hoursNotification = await this.services.scheduleService.findByRange(
      lteDate,
      gteDate,
    );
    const usersIds = [];
    const doctorsIds = [];
    for (const notification of hoursNotification) {
      usersIds.push(notification.user_id);
      doctorsIds.push(notification.doctor_id);
    }
    const users = await this.services.usersService.getMany(usersIds);
    const doctors = await this.services.doctorsService.getMany(doctorsIds);

    if (hoursNotification.length > 0) {
      await this.notify(currentDate, hoursNotification, users, doctors, false);
    }
  }
  /**
   *
   */
  async notificationDay() {
    const currentDate = moment().format('YYYY-MM-DD HH:mm:ss');
    const lteDate = moment().add('d', 1).add('minute', 2).unix();
    const gteDate = moment().add('d', 1).unix();
    const hoursNotification = await this.services.scheduleService.findByRange(
      lteDate,
      gteDate,
    );
    const usersIds = [];
    const doctorsIds = [];
    for (const notification of hoursNotification) {
      usersIds.push(notification.user_id);
      doctorsIds.push(notification.doctor_id);
    }
    const users = await this.services.usersService.getMany(usersIds);
    const doctors = await this.services.doctorsService.getMany(doctorsIds);

    if (hoursNotification.length > 0) {
      await this.notify(currentDate, hoursNotification, users, doctors, true);
    }
  }
  /**
   *
   */
  init() {
    setInterval(async () => {
      await this.notificationHour();
      await this.notificationDay();
    }, 1 * 1000 * 90);
  }
}
