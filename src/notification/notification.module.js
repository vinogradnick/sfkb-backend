import { NotificationService } from './notification.service';
import { scheduleService } from '../schedule/schedule.module';
import { usersService } from '../users/users.module';

import { doctorsService } from '../doctors/doctors.module';
export const notificationModule = new NotificationService({
  doctorsService,
  scheduleService,
  usersService,
});
