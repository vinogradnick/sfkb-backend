import mongoose from 'mongoose';

/**
 * @param {string} connectionString
 * @return {mongoose}
 */
export default function mongoProvider() {
  if (!mongoose.connection.readyState) {
    mongoose.connect(
      `mongodb+srv://sfkb:${process.env.MONGO_PASSWORD}@cluster0.dhgtj.mongodb.net/${process.env.MONGO_DATABASE}?retryWrites=true&w=majority`,
      {
        useNewUrlParser: true,
        useFindAndModify: false,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useUnifiedTopology: true,
      },
    );
  }
  return mongoose;
}
