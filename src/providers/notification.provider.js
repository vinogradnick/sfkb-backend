import fs from 'fs';
import util from 'util';

const writer = util.promisify(fs.appendFile);
/**
 * write notification to file

 * @param {string} text
 */
export default async function writeNotification(text) {
  return writer(
    'ntf.log',
    // eslint-disable-next-line max-len
    text,
  );
}
